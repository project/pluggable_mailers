CHANGELOG

7.x-1.0-beta2
=============
- Allow the caller to set the language when sending the email
- Fix an issue with the version number
- Remove a missing files[] reference in .info
- Documentation tweaks
- Add missing dependency on Chaos Tools

7.x-1.0-beta1
=============
- Initial release